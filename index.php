<?php
error_reporting(-1);
ini_set("display_errors",1);

//header("Content-Type: text/plain");
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

include 'HTMHell.php';
?>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
* {
	border: 1px dashed silver;
	margin: 5px;
}

#cool {
	background-color: red;

}
</style>
</head>
<?php
$data = json_encode(array(
	'class' => 'something',
	'id' => 'cool',
	'content' => 'stuff'
));

$listitem_image = '
{
	"src": "http://www.iconfinder.com/ajax/download/png/?id=17999&s=128",
	"width": "32",
	"selfclosing": "true"
}
';


$body = HTMHell::body();

$body->div("this is a test!")->div('here\'s another!')->h1($data);

$body->hr(json_encode(array("selfclosing" => "true")));

$body->div()->span("content!");

$ul = $body->ol();

for ($i = 0; $i <= 9; $i++) {
	$ul->li("Item ".$i)->img($listitem_image);
}

$form = $body->form();
$form->input();
$form->button("Click!");


$form->p('test');

$body->render();

?>
</html>
<?php
exit;